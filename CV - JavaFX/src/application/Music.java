package application;

import java.io.File;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;

public class Music {
	
	public static Music instance = new Music();
	
	public static Music getInstance() {
		
		playMusic();
		return instance;
	}

	
	public static void playMusic() {
		String path = "C:\\Users\\Marcin\\Desktop\\Java\\CV\\songcv.mp3";
		Media media = new Media(new File(path).toURI().toString());
		MediaPlayer mediaPlayer = new MediaPlayer(media);
		mediaPlayer.setAutoPlay(true);
		MediaView mediaView = new MediaView(mediaPlayer);
		mediaView.getMediaPlayer();
	}

}
