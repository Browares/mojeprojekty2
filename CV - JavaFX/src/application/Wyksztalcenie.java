package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class Wyksztalcenie implements Initializable {
	@FXML
	private Button button;

	@FXML
	void nextScene(ActionEvent event) throws IOException {
		changeScene(button, "Sample5.fxml");
	}

	public void changeScene(Button button, String fxml) throws IOException {
		Stage stage;
		Parent root;
		// przypisanie nowej sceny do buttona
		stage = (Stage) button.getScene().getWindow();
		// zaladowanie nowego pliku fxml
		root = FXMLLoader.load(getClass().getResource(fxml));
		// create a new scene with root and set the stage
		Scene scene = new Scene(root, 400, 400);
		stage.setScene(scene);
		stage.show();
		root.setStyle("-fx-background-color: #E3CEF6;");
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}
}